import java.util.Scanner;

public class Examen1JavierPeralta {

	public static void main(String[] args) {
		Scanner miScanner = new Scanner(System.in);
		String l�nea, palabraInv;
		Integer men�, ladoTri, contNums;
		Double num3, suma;
		do {
			System.out.println("");
			System.out.println("MENU");
			System.out.println("====");
			System.out.println("1. Palabra invertida");
			System.out.println("2. Tri�ngulo n�meros");
			System.out.println("3. Suma/Media");
			System.out.println("4. Salir");
			System.out.println("");
			System.out.print("Introduce una opci�n: ");
			l�nea = miScanner.nextLine();
			men� = Integer.parseInt(l�nea);
			System.out.println("");
			switch (men�){
			case 1:
				System.out.print("Introduce una palabra: ");
				palabraInv = miScanner.nextLine().toUpperCase();
				System.out.println("");
				for (int i = palabraInv.length() -1; i >= 0; i--) {
					System.out.print(palabraInv.charAt(i));
				}
				break;
			case 2:
				do  {
					System.out.print("Introduce el tama�o del lado del tri�ngulo: ");
				l�nea = miScanner.nextLine();
				ladoTri = Integer.parseInt(l�nea);
				System.out.println("");
				if (ladoTri <= 0) {
					System.out.println("El n�mero debe ser positivo");
					System.out.println("");
				}
				}while (ladoTri <= 0);
				for(int i = ladoTri; i >= 1; i--) {
						for(int j = 1; j <= i; j++) {
							System.out.print(j + " ");
						}
						System.out.println("");
					}
				break;
			case 3:
				contNums = 0;
				suma = 0d;
				do {
				System.out.println("Introduce un n�mero (Negativo para salir)");
				l�nea = miScanner.nextLine();
				num3 = Double.parseDouble(l�nea);
				if(num3 >= 0){
					suma = (suma + num3);
					contNums += 1;
				}
				}while (num3 >= 0);
				System.out.println("La suma de los n�meros es: " + suma);
				System.out.println("La media de los n�meros es: " + (suma / contNums));
				break;
			case 4:
				System.out.println("Saliendo...");
				break;
			default:
				System.out.println("Opci�n incorrecta");
			}
			System.out.println("");
		}while (men� != 4);
	}

}
